FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine3.16 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine3.16 AS build
WORKDIR /src
COPY test-code.csproj .
RUN dotnet restore "test-code.csproj"
RUN dotnet tool install --tool-path /tools dotnet-dump
COPY . .
RUN dotnet build "test-code.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "test-code.csproj" -c Release -o /app/publish

FROM base AS final

WORKDIR /tools
COPY --from=build /tools .

WORKDIR /app
# Change timezone to America/Barbados (GMT-4)
ENV TZ America/Barbados
RUN apk add --no-cache tzdata
RUN cp /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN sed -i '1i openssl_conf = default_conf' /etc/ssl/openssl.cnf && echo -e "\n[ default_conf ]\nssl_conf = ssl_sect\n[ssl_sect]\nsystem_default = system_default_sect\n[system_default_sect]\nMinProtocol = TLSv1\nCipherString = DEFAULT:@SECLEVEL=1" >> /etc/ssl/openssl.cnf
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "test-code.dll"]
